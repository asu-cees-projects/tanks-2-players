﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using Battle_of_the_Tanks;

namespace Battle_of_the_Tanks.Intro
{
    class HomePnl : Panel
    {
        bool clientC, hostC, hostIP,isHost;
        String cIP;
        Image background;
        Image title, play, host, client, exit, check, hip, tip;
        public imageDim C, H, HIP, P, T, E, R, N;
        Pen p; Timer t;
        Color c;
        int screenWidth = IntroScreen.screenWidth;
        int screenHeight = IntroScreen.screenHeight;
        Form IntroForm;
        public HomePnl(Form IntroForm)
        {
            this.IntroForm = IntroForm;
            this.DoubleBuffered = true;
            this.Width = screenWidth;
            this.Height = screenHeight;
            this.MouseClick += HomePnl_MouseClick;

            N = new imageDim();
            t = new Timer();
            t.Interval = 1;
            t.Tick += T_Tick;
            t.Start();

            clientC = false;
            hostC = false;
            hostIP = false;
            HIP = new imageDim();

            background = Image.FromFile(@"img\home\background.jpg");
            title = Image.FromFile(@"img\home\title.png");
            play = Image.FromFile(@"img\home\play.png");
            host = Image.FromFile(@"img\home\host.png");
            client = Image.FromFile(@"img\home\client.png");
            exit = Image.FromFile(@"img\home\x.png");
            check = Image.FromFile(@"img\home\check.png");
            hip = Image.FromFile(@"img\home\host ip.png");
            tip = Image.FromFile(@"img\home\iptrans.png");

            this.BackgroundImage = background;
        }

        private void T_Tick(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        private void HomePnl_MouseClick(object sender, MouseEventArgs e)
        {
            if ((e.X >= screenWidth * 0.9f) && (e.X <= screenWidth) && (e.Y >= screenHeight * 0.01f) && (e.Y <= screenHeight * 0.08f))
                    Environment.Exit(Environment.ExitCode);
            if ((e.X >= screenWidth * 0.772f) && (e.X <= screenWidth * 0.872f)
                && (e.Y >= screenHeight * 0.1f) && (e.Y <= screenHeight * 0.2f))
            {
                if (clientC == false)
                {
                    clientC = true;
                    hostC = false;
                    hostIP = true;
                    drawNetworkCheck(1);
                    drawEnterIP();
                    IntroScreen.ipAdd.Enabled = true;
                    IntroScreen.ipAdd.Visible = true;
                    isHost = false;
                }
            }
            if ((e.X >= screenWidth * 0.75f) && (e.X <= screenWidth * 0.85f)
                && (e.Y >= screenHeight * 0.5f) && (e.Y <= screenHeight * 0.6f))
            {
                if (hostC == false)
                {
                    hostC = true;
                    IntroScreen.ipAdd.Enabled = false;
                    IntroScreen.ipAdd.Visible = false;
                    clientC = false;
                    hostIP = false;
                    drawEnterIP();
                    drawNetworkCheck(2);
                    isHost = true;
                }

            }
           
            if ((e.X >= screenWidth * 0.55f) && (e.X <= screenWidth * 0.8f) && (e.Y >= screenHeight * 0.75f) && (e.Y <= screenHeight * 0.95f))
            {
                if(isHost == true)
                {
                    GameForm gfH = new GameForm(6666, IntroForm);
                   
                    gfH.Show();
                    IntroForm.Hide();

                }
                else if(isHost == false)
                {
                    cIP = IntroScreen.ipAdd.Text;
                    GameForm gfH = new GameForm(6666, cIP , IntroForm);

                    gfH.Show();
                    IntroForm.Hide();
                }
            }

        }

        public void drawTitle()
        {
            T = new imageDim();
            T.x = screenWidth * 0.05f;
            T.y = screenHeight * 0.1f;
            T.w = screenWidth * 0.4f;
            T.h = screenHeight * 0.8f;
        }
        public void drawClient()
        {
            C = new imageDim();
            C.x = screenWidth * 0.5f;
            C.y = screenHeight * 0.1f;
            C.w = screenWidth * 0.25f;
            C.h = screenHeight * 0.1f;
        }
        public void drawHost()
        {
            H = new imageDim();
            H.x = screenWidth * 0.5f;
            H.y = screenHeight * 0.5f;
            H.w = screenWidth * 0.25f;
            H.h = screenHeight * 0.1f;
        }
        public void drawEnterIP()
        {
            HIP.x = screenWidth * 0.5f;
            HIP.y = screenHeight * 0.25f;
            HIP.w = screenWidth * 0.4f;
            HIP.h = screenHeight * 0.085f;
        }
        public void drawPlay()
        {
            P = new imageDim();
            P.x = screenWidth * 0.55f;
            P.y = screenHeight * 0.75f;
            P.w = screenWidth * 0.25f;
            P.h = screenHeight * 0.2f;
        }
        public void drawExit()
        {
            E = new imageDim();
            E.x = screenWidth * 0.9f;
            E.y = screenHeight * 0.01f;
            E.w = screenWidth * 0.1f;
            E.h = screenWidth * 0.07f;
        }
        public void drawRectangle(int x)
        {
            R = new imageDim();
            switch (x)
            {
                case 1:
                    c = Color.FromArgb(255, 0, 0);
                    R.x = screenWidth * 0.772f;
                    R.y = screenHeight * 0.1f;
                    R.w = screenHeight * 0.1f;
                    R.h = screenHeight * 0.1f;
                    break;
                case 2:
                    c = Color.FromArgb(0, 0, 255);
                    R.x = screenWidth * 0.772f;
                    R.y = screenHeight * 0.5f;
                    R.w = screenHeight * 0.1f;
                    R.h = screenHeight * 0.1f;
                    break;
            }
            p = new Pen(c, 12.5f);
        }
        public void drawNetworkCheck(int x)
        {
            switch (x)
            {
                case 1:
                    N.x = screenWidth * 0.772f;
                    N.y = screenHeight * 0.1f;
                    N.w = screenHeight * 0.1f;
                    N.h = screenHeight * 0.1f;
                    break;
                case 2:
                    N.x = screenWidth * 0.772f;
                    N.y = screenHeight * 0.5f;
                    N.w = screenHeight * 0.1f;
                    N.h = screenHeight * 0.1f;
                    break;
            }
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.DrawImage(title, T.x, T.y, T.w, T.h);
            e.Graphics.DrawImage(client, C.x, C.y, C.w, C.h);
            e.Graphics.DrawImage(host, H.x, H.y, H.w, H.h);
            e.Graphics.DrawImage(play, P.x, P.y, P.w, P.h);
            e.Graphics.DrawImage(exit, E.x, E.y, E.w, E.h);
            drawRectangle(1);
            e.Graphics.DrawRectangle(p, R.x, R.y, R.w, R.h);
            drawRectangle(2);
            e.Graphics.DrawRectangle(p, R.x, R.y, R.w, R.h);
            if (clientC == true)
                e.Graphics.DrawImage(check, N.x, N.y, N.w, N.h);
            if (hostC == true)
                e.Graphics.DrawImage(check, N.x, N.y, N.w, N.h);
            if (hostIP == true)
            {

                e.Graphics.DrawImage(hip, HIP.x, HIP.y, HIP.w, HIP.h);
            }
            if (hostIP == false)
            {
                e.Graphics.DrawImage(tip, HIP.x, HIP.y, HIP.w, HIP.h);
            }
        }
    }
}
   