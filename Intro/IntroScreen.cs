﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battle_of_the_Tanks.Intro
{
    public partial class IntroScreen : Form
    {
        HomePnl home;
        public static Form IntroScreenHide;
        public static TextBox ipAdd;
        public static int screenWidth;
        public static int screenHeight;
        Timer t;
        public IntroScreen()
        {
            WindowState = FormWindowState.Maximized;
            FormBorderStyle = FormBorderStyle.None;
            screenWidth = Screen.PrimaryScreen.Bounds.Width;
            screenHeight = Screen.PrimaryScreen.Bounds.Height;
            home = new HomePnl(this);
            IntroScreenHide = this;
            home.drawEnterIP();
            InitializeComponent();
            homescreenInit();
            ipAdd = new TextBox();
            ipAdd.Enabled = false;
            ipAdd.Visible = false;
            ipAdd.TextAlign = HorizontalAlignment.Center;
            ipAdd.SetBounds((int)home.HIP.x, (int)(home.HIP.y + 0.1f * screenHeight), (int)home.HIP.w, (int)home.HIP.y);
            home.Controls.Add(ipAdd);
            this.Controls.Add(home);
        }

        private void homescreenInit()
        {
            home.drawTitle();

            home.drawClient();

            home.drawHost();

            home.drawPlay();

            home.drawExit();
        }
    }
}
