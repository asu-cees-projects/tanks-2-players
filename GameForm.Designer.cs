﻿namespace Battle_of_the_Tanks
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Player1Health = new System.Windows.Forms.ProgressBar();
            this.Player2Health = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // Player1Health
            // 
            this.Player1Health.BackColor = System.Drawing.Color.Maroon;
            this.Player1Health.ForeColor = System.Drawing.Color.Brown;
            this.Player1Health.Location = new System.Drawing.Point(490, 419);
            this.Player1Health.Maximum = 1000;
            this.Player1Health.Name = "Player1Health";
            this.Player1Health.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Player1Health.RightToLeftLayout = true;
            this.Player1Health.Size = new System.Drawing.Size(288, 23);
            this.Player1Health.TabIndex = 0;
            this.Player1Health.Value = 1000;
            // 
            // Player2Health
            // 
            this.Player2Health.Location = new System.Drawing.Point(-4, 420);
            this.Player2Health.Maximum = 1000;
            this.Player2Health.Name = "Player2Health";
            this.Player2Health.Size = new System.Drawing.Size(288, 23);
            this.Player2Health.TabIndex = 1;
            this.Player2Health.Value = 1000;
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 444);
            this.Controls.Add(this.Player2Health);
            this.Controls.Add(this.Player1Health);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "GameForm";
            this.Text = "Battle of the Tanks v0.1";
            this.Load += new System.EventHandler(this.GameForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ProgressBar Player1Health;
        public System.Windows.Forms.ProgressBar Player2Health;


    }
}

