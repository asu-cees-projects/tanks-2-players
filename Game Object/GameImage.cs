﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Battle_of_the_Tanks.GameObject
{
    abstract class GameImage
    {
        protected float x, y;
        public float X
        {
            get { return this.x; }
            set { this.x = value; }
        }
        public float Y
        {
            get { return this.y; }
            set { this.y = value; }
        }
        protected Image i;

        public GameImage(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
        public abstract void draw(Graphics g);

    }
}
