﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Drawing;

namespace Battle_of_the_Tanks.GameObject
{
    class Shield
    {
        Image i;
        bool isPlayer1;
        GamePanel gp;
        
        public Shield(bool isPlayer1,GamePanel gp)
        {
            this.isPlayer1 = isPlayer1;
            this.gp = gp;
            if(isPlayer1)
                this.i = Image.FromFile(@"img\shield2.png");
            else
                this.i = Image.FromFile(@"img\shield.png");
        }

        public void activate()
        {
            if(this.isPlayer1)
            {
                gp.p1.isShielded = true;
                Thread.Sleep(10000);
                gp.p1.isShielded = false;
            }
            else
            {
                gp.p2.isShielded = true;
                Thread.Sleep(10000);
                gp.p2.isShielded = false;
            }
        }

        public void drawShield(Graphics g)
        {
            if (this.isPlayer1)
            {
                g.DrawImage(i, (float)(((gp.p1.X - 0.05)) * GameForm.screenWidth), (float)((gp.p1.Y) * GameForm.screenHeight), (float)(0.03 * GameForm.screenWidth), (float)(0.1 * GameForm.screenHeight));
            
            }
            else
            {
                g.DrawImage(i, (float)(((gp.p2.X + 0.12)) * GameForm.screenWidth), (float)((gp.p2.Y) * GameForm.screenHeight), (float)(0.03 * GameForm.screenWidth), (float)(0.1 * GameForm.screenHeight));

            }
        }       
        }
    }

