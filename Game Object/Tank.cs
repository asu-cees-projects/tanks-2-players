﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Drawing;

namespace Battle_of_the_Tanks.GameObject
{
    class Tank : GameImage
    {
        int life;


        public bool isShielded;
        public bool superWep;
        public bool isHealth;

        public int Life
        {
            get { return this.life; }
            set { this.life = value; }
        }

        public int player;

        public List<Projectile> ammo;

        public Tank(float x, float y, int player) : base(x, y)
        {
            this.player = player;
            switch (player)
            {
                case 1:
                    this.i = Image.FromFile(@"img\bluetank.png"); 
                    break;
                case 2:
                    this.i = Image.FromFile(@"img\redtank.png"); 
                    break;
            }
            this.life = 1000;
            ammo = new List<Projectile>();
        }

        public void fire()
        {
            switch (player)
            {
                case 1:
                    if (this.superWep)
                    {
                        this.ammo.Add(new Projectile(this.x, this.y, 3));
                    }
                    else
                    {
                        this.ammo.Add(new Projectile((this.x), (this.y), 1));
                    }
                    break;
                case 2:
                    if (this.superWep)
                    {
                        this.ammo.Add(new Projectile(this.x, this.y, 4));
                    }
                    else
                    {
                        this.ammo.Add(new Projectile((x), (y), 2));
                    }
                    break;
            }
        }

        public override void draw(Graphics g)
        {
            g.DrawImage(i,x * GameForm.screenWidth, y * GameForm.screenHeight, GameForm.screenWidth / 10, GameForm.screenHeight / 10);//replace x with x*width and width with percentage of screen
            foreach (Projectile p in this.ammo)
            {
                p.draw(g);
            }
        }

        public void moveAmmo()
        {
            switch (player)
            {
                case 1:

                    foreach (Projectile p in ammo)
                    {
                        p.X -= 0.01f;   
                    }
                    break;
                case 2:

                    foreach (Projectile p in ammo)
                    {
                        p.X += 0.01f;  
                    }
                    break;
            }
        }

        public void erase(Projectile p)
        {
            if (this.ammo.Contains(p))
                this.ammo.Remove(p);
        }

        public void activateSuper()
        {
                this.superWep = true;
                Thread.Sleep(10000);
                this.superWep = false;
        }

        
    }
}
