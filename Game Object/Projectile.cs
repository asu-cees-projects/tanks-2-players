﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Battle_of_the_Tanks.GameObject
{
    class Projectile : GameImage
    {
        int damage;
        public int Damage
        {
            get { return this.damage; }
            set { this.damage = value; }
        }

        public Projectile(float x, float y, int weapon)
            : base(x, y)
        {
            switch (weapon)
            {
                case 1:
                    this.i = Image.FromFile(@"img\blueProjectile.png");
                    this.damage = 1;
                    break;
                case 2:
                    this.i = Image.FromFile(@"img\redProjectile.png");
                    this.damage = 1;
                    break;
                case 3:
                    this.i = Image.FromFile(@"img\fireProjectile.png");
                    this.damage = 5;
                    break;
                case 4:
                    this.i = Image.FromFile(@"img\fireProjectile2.png");
                    this.damage = 5;
                    break;
            }
        }

        public override void draw(Graphics g)
        {
            g.DrawImage(i, x * GameForm.screenWidth, ((y+0.029f) * GameForm.screenHeight), GameForm.screenWidth / 30, GameForm.screenHeight / 30);
        }
    }
}
