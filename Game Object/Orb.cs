﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Battle_of_the_Tanks.GameObject
{
    class Orb:GameImage
    {
        public int type;
        public bool isConsumable;
        public Orb(float x, float y, int type):base(x,y)
        {
            this.type = type;
            if (type == 0)
            {
                this.i = Image.FromFile(@"img\redOrb.png");
            }
            else if (type == 1)
            {
                this.i = Image.FromFile(@"img\fireOrb.png");
            }
            else if (type == 2)
            {
                this.i = Image.FromFile(@"img\greenOrb.png");
            }
            
        }
        public override void draw(Graphics g)
        {
            if(this.isConsumable)
                g.DrawImage(i, (float)(this.x * GameForm.screenWidth), (float)(this.y * GameForm.screenHeight), (float)(0.03 * GameForm.screenWidth), (float)(0.03 * GameForm.screenWidth));
        }

       
        
    }
}
