﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battle_of_the_Tanks.Exit
{
    public partial class ExitScreen : Form
    {
        public static int screenWidth;
        public static int screenHeight;
        public static Form es;
        ExitPnl exit;
        int PlayerStatus;
        public Form IbwdForm;
        public static Form intro;
        public ExitScreen(int PlayerStatus, Form IbwdForm)
        {
            this.IbwdForm = IbwdForm;
            es = this;
            intro = this.IbwdForm;
            WindowState = FormWindowState.Maximized;
            FormBorderStyle = FormBorderStyle.None;
            screenWidth = Screen.PrimaryScreen.Bounds.Width;
            screenHeight = Screen.PrimaryScreen.Bounds.Height;

            exit = new ExitPnl();

            this.PlayerStatus = PlayerStatus;
            InitializeComponent();
            this.Controls.Add(exit);
            exitScreenInit();
        }
        private void exitScreenInit()
        {
            exit.drawPlayerState(PlayerStatus);
        }
    }
}



