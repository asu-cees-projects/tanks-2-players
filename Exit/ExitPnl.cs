﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Battle_of_the_Tanks.Intro;

namespace Battle_of_the_Tanks.Exit
{
    class ExitPnl : Panel
    {
        Timer t;
        Image background, gameover, player1, player2, won, lost, exit, home, ps, wol;
        imageDim G, Player, State, E, H;
        int screenWidth = IntroScreen.screenWidth;
        int screenHeight = IntroScreen.screenHeight;
        public ExitPnl()
        {
            this.DoubleBuffered = true;
            this.Width = screenWidth;
            this.Height = screenHeight;
            this.MouseClick += ExitPnl_MouseClick;

            drawGameOver();
            drawHome();
            drawExit();

            t = new Timer();
            t.Interval = 10;
            t.Tick += T_Tick;
            t.Start();

            background = Image.FromFile(@"img\exit\background.jpg");
            gameover = Image.FromFile(@"img\exit\gameover.png");
            player1 = Image.FromFile(@"img\exit\p1.png");
            player2 = Image.FromFile(@"img\exit\p2.png");
            won = Image.FromFile(@"img\exit\won.png");
            lost = Image.FromFile(@"img\exit\lost.png");
            exit = Image.FromFile(@"img\exit\exit.png");
            home = Image.FromFile(@"img\exit\home.png");

            this.BackgroundImage = background;
        }
        private void ExitPnl_MouseClick(object sender, MouseEventArgs e)
        {
            if ((e.X >= screenWidth * 0.1f) && (e.X <= screenWidth * 0.3f) && (e.Y >= screenHeight * 0.8f) && (e.Y <= screenHeight * 0.9f))
            {
                
                Application.Restart();
            }
            if ((e.X >= screenWidth * 0.7f) && (e.X <= screenWidth * 0.9f) && (e.Y >= screenHeight * 0.8f) && (e.Y <= screenHeight * 0.9f))
            {
                Environment.Exit(Environment.ExitCode);
            }
        }

        private void T_Tick(object sender, EventArgs e)
        {
            this.Invalidate();
        }
        public void drawGameOver()
        {
            G = new imageDim();
            G.x = screenWidth * 0.15f;
            G.y = screenHeight * 0.05f;
            G.w = screenWidth * 0.7f;
            G.h = screenHeight * 0.15f;
        }
        public void drawHome()
        {
            H = new imageDim();
            H.x = screenWidth * 0.1f;
            H.y = screenHeight * 0.8f;
            H.w = screenWidth * 0.2f;
            H.h = screenHeight * 0.1f;
        }
        public void drawExit()
        {
            E = new imageDim();
            E.x = screenWidth * 0.7f;
            E.y = screenHeight * 0.8f;
            E.w = screenWidth * 0.2f;
            E.h = screenHeight * 0.1f;
        }
        public void drawPlayerState(int x)
        {
            Player = new imageDim();
            Player.x = screenWidth * 0.3f;
            Player.y = screenHeight * 0.35f;
            Player.w = screenWidth * 0.4f;
            Player.h = screenHeight * 0.1f;
            State = new imageDim();
            State.x = screenWidth * 0.35f;
            State.y = screenHeight * 0.6f;
            State.w = screenWidth * 0.3f;
            State.h = screenHeight * 0.1f;
            switch (x)
            {
                case 1://p1 win
                    ps = player1;
                    wol = won;
                    break;
                case 2://p1 lose
                    ps = player1;
                    wol = lost;
                    break;
                case 3://p2 win
                    ps = player2;
                    wol = won;
                    break;
                case 4://p2 lose
                    ps = player2;
                    wol = lost;
                    break;
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.DrawImage(gameover, G.x, G.y, G.w, G.h);
            e.Graphics.DrawImage(home, H.x, H.y, H.w, H.h);
            e.Graphics.DrawImage(exit, E.x, E.y, E.w, E.h);
            e.Graphics.DrawImage(ps, Player.x, Player.y, Player.w, Player.h);
            e.Graphics.DrawImage(wol, State.x, State.y, State.w, State.h);

        }
    }
}
