﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;

using Battle_of_the_Tanks.Exit;

using Battle_of_the_Tanks.GameObject;

namespace Battle_of_the_Tanks
{
    class GamePanel : Panel
    {
        ExitScreen es;

        public Tank p1;
        public Tank p2;

        public Shield s1;
        public Shield s2;

        public Orb hOrb;
        public Orb pOrb;
        public Orb sOrb;

        public int orbState;
        int orbGenCount;

        System.Windows.Forms.Timer repaintTimer;

        bool isPlayer1;
        bool hasLost;
        bool hasWon;

        GameForm g;
        
        Random rand = new Random();

        public int incomingMessID;

        public GamePanel(GameForm g, bool isPlayer1)
        {
            this.DoubleBuffered = true;
            this.BackgroundImage = Image.FromFile(@"img\bg3.jpg");
            this.Dock = DockStyle.Fill;
            this.g = g;
            this.isPlayer1 = isPlayer1;
            this.incomingMessID = 0;

            start();

        }

        void GamePanel_Paint(object sender, PaintEventArgs e)
        {
            
        }
        public void start()
        {
            p1 = new Tank(0.89f, 0.5f, 1);
            p2 = new Tank(0f, 0.5f, 2);
            s1 = new Shield(true, this);
            s2 = new Shield(false, this);
            hOrb = new Orb(0.5f, 0.2f, 0);
            pOrb = new Orb(0.5f, 0.5f, 1);
            sOrb = new Orb(0.5f, 0.8f, 2);
            orbState = 0;
            orbGenCount = 0;
            repaintTimer = new System.Windows.Forms.Timer();
            repaintTimer.Interval = 10;
            repaintTimer.Tick += repaintTimer_Tick;
            repaintTimer.Start();
         
        }

        void repaintTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                updateEnemy();

                
                winner();
                damageTank();
                moveTanks();
                p1.moveAmmo();
                p2.moveAmmo();
                if (isPlayer1)
                {
                    if (orbGenCount == 50000)
                    {
                        orbGenCount = 0;
                        generateOrb();
                    }
                    else
                    {
                        orbGenCount += 50;

                    }
                    orbCollision(p1, hOrb);
                    orbCollision(p1, pOrb);
                    orbCollision(p1, sOrb);
                    orbCollision(p2, hOrb);
                    orbCollision(p2, pOrb);
                    orbCollision(p2, sOrb);
                    healthIncrement(p2);
                }
                else
                {
                    healthIncrement(p1);
                    p2.isHealth = false;
                }

                this.Invalidate();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void activateShield(bool whichP)
        {

            if (whichP)
            {
                Thread t = new Thread(s1.activate);
                t.Start();
            }
            else
            {
                Thread t = new Thread(s2.activate);
                t.Start();
            }
        }

        public void superActivate(bool whichP)
        {
            if (whichP)
            {
                Thread t = new Thread(p1.activateSuper);
                t.Start();
            }
            else
            {
                Thread t = new Thread(p2.activateSuper);
                t.Start();
            }
        }

        

        public void updateEnemy()
        {
            if (g.incomingMess._ID > incomingMessID)
            {
                if (isPlayer1)
                {

                    p2.X = g.incomingMess.X;
                    p2.Y = g.incomingMess.Y;
                    incomingMessID = g.incomingMess._ID;
                    p1.Life = g.incomingMess.LIFE;
                    try
                    {
                        g.Player1Health.Value = g.incomingMess.LIFE;
                    }
                    catch(Exception e)
                    {
                        g.Player1Health.Value = 0;
                    }
                    if (g.incomingMess.FIRED)
                    {
                        p2.fire();
                    }

                }
                else
                {
                    p1.X = g.incomingMess.X;
                    p1.Y = g.incomingMess.Y;
                    incomingMessID = g.incomingMess._ID;
                    p2.Life = g.incomingMess.LIFE;
                    try
                    {
                        g.Player2Health.Value = g.incomingMess.LIFE;
                    }
                    catch(Exception e)
                    {
                        g.Player2Health.Value = 0;
                    }
                    if (g.incomingMess.FIRED)
                    {
                        p1.fire();
                    }
                    p1.isShielded = g.incomingMess.p1shield;
                    p2.isShielded = g.incomingMess.p2shield;

                    p1.superWep = g.incomingMess.p1super;
                    p2.superWep = g.incomingMess.p2super;

                    p1.isHealth = g.incomingMess.p1hei;
                    p2.isHealth = g.incomingMess.p2hei;

                    switch (g.incomingMess.orbstate)
                    {
                        case 0:
                            hOrb.isConsumable = false;
                            pOrb.isConsumable = false;
                            sOrb.isConsumable = false;
                            break;
                        case 1:
                            hOrb.isConsumable = true;
                            pOrb.isConsumable = false;
                            sOrb.isConsumable = false;
                            break;
                        case 2:
                            hOrb.isConsumable = false;
                            pOrb.isConsumable = true;
                            sOrb.isConsumable = false;
                            break;
                        case 3:
                            hOrb.isConsumable = false;
                            pOrb.isConsumable = false;
                            sOrb.isConsumable = true;
                            break;
                        case 4:
                            hOrb.isConsumable = true;
                            pOrb.isConsumable = true;
                            sOrb.isConsumable = false;
                            break;
                        case 5:
                            hOrb.isConsumable = false;
                            pOrb.isConsumable = true;
                            sOrb.isConsumable = true;
                            break;
                        case 6:
                            hOrb.isConsumable = true;
                            pOrb.isConsumable = false;
                            sOrb.isConsumable = true;
                            break;
                        case 7:
                            hOrb.isConsumable = true;
                            pOrb.isConsumable = true;
                            sOrb.isConsumable = true;
                            break;
                    }
                }
            }
        }
        public void winner()
        {
            if (isPlayer1)
            {
                if (p1.Life <= 0)
                {
                    repaintTimer.Stop();
                    g.thisEnd.Close();
                    if (!hasLost)
                    {
                        hasLost = true;
                        es = new ExitScreen(2, GameForm.IbwdForm);
                        g.t1.Abort();
                        g.Close();
                        g.Dispose();
                        es.Show();
                    }


                }
                else if (p2.Life <= 0)
                {
                    repaintTimer.Stop();
                    g.thisEnd.Close();
                    if (!hasWon)
                    {
                        hasWon = true;
                        es = new ExitScreen(1, GameForm.IbwdForm);
                        g.t1.Abort();
                        g.Close();
                        g.Dispose();
                        es.Show();
                    }

                }
            }
            else
            {
                if (p1.Life <= 0)
                {
                    repaintTimer.Stop();
                    g.thisEnd.Close();
                    if (!hasWon)
                    {
                        hasWon = true;
                        es = new ExitScreen(3, GameForm.IbwdForm);
                        g.t2.Abort();
                        g.Close();
                        g.Dispose();
                        es.Show();
                    }


                    
                }
                else if (p2.Life <= 0)
                {
                    repaintTimer.Stop();
                    g.thisEnd.Close();
                    if (!hasLost)
                    {
                        hasLost = true;
                        es = new ExitScreen(4, GameForm.IbwdForm);
                        g.t2.Abort();
                        g.Close();
                        g.Dispose();
                        es.Show();
                    }
    
                }
            }
        }
        public void moveTanks()
        {
            if (isPlayer1)
            {
                if (KeyInputManager.getKeyState(Keys.Up))
                {
                    if (p1.Y >= 0)
                        p1.Y -= 0.01f;
                }
                if (KeyInputManager.getKeyState(Keys.Down))
                {
                    if (p1.Y <= (0.82))
                        p1.Y += 0.01f;
                }
                if (KeyInputManager.getKeyState(Keys.Left))
                {
                    if (p1.X >= 0)

                        p1.X -= 0.01f;
                }
                if (KeyInputManager.getKeyState(Keys.Right))
                {
                    if (p1.X <= (0.89))
                        p1.X += 0.01f;
                }
                if (KeyInputManager.getKeyState(Keys.Space))
                {

                    p1.fire();

                }
            }
            else
            {
                if (KeyInputManager.getKeyState(Keys.Up))
                {
                    if (p2.Y >= 0)
                        p2.Y -= 0.01f;
                }
                if (KeyInputManager.getKeyState(Keys.Down))
                {
                    if (p2.Y <= (0.82))
                        p2.Y += 0.01f;
                }
                if (KeyInputManager.getKeyState(Keys.Left))
                {
                    if (p2.X >= 0)

                        p2.X -= 0.01f;
                }
                if (KeyInputManager.getKeyState(Keys.Right))
                {
                    if (p2.X <= (0.89))
                        p2.X += 0.01f;
                }
                if (KeyInputManager.getKeyState(Keys.Space))
                {

                    p2.fire();

                }

            }
        }
        public void damageTank()
        {
            
            List<Projectile> toBeDeleted = new List<Projectile>();
            foreach (Projectile p in p1.ammo)
            {


                if (!p2.isShielded)
                {
                    if (p2.X < p.X && p.X < (p2.X + 0.1f) && (p2.Y - 0.05f) < p.Y && p.Y < (p2.Y + 0.1f))
                    {
                        toBeDeleted.Add(p);

                        if (isPlayer1)
                        {
                            p2.Life -= p.Damage;
                            try
                            {
                                g.Player2Health.Increment(-1 * p.Damage);
                            }
                            catch (Exception e)
                            {
                                g.Player2Health.Value = 0;
                            }
                        }

                        winner();
                    }
                }
                else
                {
                    if (p2.X < p.X && p.X < (p2.X + 0.15f) && (p2.Y - 0.05f) < p.Y && p.Y < (p2.Y + 0.1f))
                    {
                        toBeDeleted.Add(p);
                    }

                }
            }
            foreach (Projectile pt in toBeDeleted)
            {
                p1.erase(pt);
            }
            
            List<Projectile> toBeDeleted2 = new List<Projectile>();
            foreach (Projectile p in p2.ammo)
            {
                if (!p1.isShielded)
                {
                    if (p1.X < p.X && p.X < (p1.X + 0.1f) && (p1.Y - 0.05f) < p.Y && p.Y < (p1.Y + 0.1f))
                    {
                        toBeDeleted2.Add(p);

                        if (!isPlayer1)
                        {
                            p1.Life -= p.Damage;
                            try
                            {
                                g.Player1Health.Increment(-1 * p.Damage);
                            }
                            catch (Exception e)
                            {
                                g.Player1Health.Value = 0;
                            }
                        }

                        winner();
                    }
                }
                else
                {
                    if (p1.X-0.065f < p.X && p.X < (p1.X + 0.1f) && (p1.Y - 0.05f) < p.Y && p.Y < (p1.Y + 0.1f))
                    {
                        toBeDeleted2.Add(p);
                    }

                }

            }
            foreach (Projectile pt in toBeDeleted2)
            {
                p2.erase(pt);
            }

        }
        public void p1healthFix()
        {
            p1.isHealth = true;
            Thread.Sleep(20);
            p1.isHealth = false;
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (p1 != null && p2 != null && s1 != null)
            {
                p1.draw(e.Graphics);
                p2.draw(e.Graphics);
                if (p1.isShielded)
                    s1.drawShield(e.Graphics);
                if (p2.isShielded)
                    s2.drawShield(e.Graphics);
                hOrb.draw(e.Graphics);
                pOrb.draw(e.Graphics);
                sOrb.draw(e.Graphics);
        

            }
        }
        private void GamePanel_Load(object sender, EventArgs e)
        {

        }
        public void healthIncrement(Tank tn)
        {
            if (tn.isHealth && tn.Life != 1000 && !((tn.Life + 100) > 1000))
            {
                tn.Life += 100;
                if(tn.player==1)
                {
                    g.Player1Health.Increment(100);
                }
                else
                {
                    g.Player2Health.Increment(100);
                }
                tn.isHealth = false;
            }
            tn.isHealth = false;

        }
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.ResumeLayout(false);

        }




        public void orbCollision(Tank tn,Orb o)
        {
            if (o.isConsumable)
            {
                if ((o.X + 0.03) <= (tn.X + 0.1) && o.X + 0.03 >= tn.X && (o.Y + 0.03) <= (tn.Y + 0.1) && o.Y + 0.03 >= tn.Y)
                {
                    if (tn.player == 1)
                    {
                        switch (o.type)
                        {
                            case 0:
                                Thread t = new Thread(p1healthFix);
                                t.Start();
                                switch (this.orbState)
                                {
                                    case 1:
                                        hOrb.isConsumable = false;
                                        this.orbState = 0;
                                        break;
                                    case 4:
                                        hOrb.isConsumable = false;
                                        this.orbState = 2;
                                        break;
                                    case 6:
                                        hOrb.isConsumable = false;
                                        this.orbState = 3;
                                        break;
                                    case 7:
                                        hOrb.isConsumable = false;
                                        this.orbState = 5;
                                        break;
                                    default:
                                        break;
                                }

                                break;
                            case 1:
                                superActivate(true);
                                switch (this.orbState)
                                {
                                    case 2:
                                        pOrb.isConsumable = false;
                                        this.orbState = 0;
                                        break;
                                    case 4:
                                        pOrb.isConsumable = false;
                                        this.orbState = 1;
                                        break;
                                    case 5:
                                        pOrb.isConsumable = false;
                                        this.orbState = 3;
                                        break;
                                    case 7:
                                        pOrb.isConsumable = false;
                                        this.orbState = 6;
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case 2:
                                activateShield(true);
                                switch (this.orbState)
                                {
                                    case 3:
                                        sOrb.isConsumable = false;
                                        this.orbState = 0;
                                        break;
                                    case 5:
                                        sOrb.isConsumable = false;
                                        this.orbState = 2;
                                        break;
                                    case 6:
                                        sOrb.isConsumable = false;
                                        this.orbState = 1;
                                        break;
                                    case 7:
                                        sOrb.isConsumable = false;
                                        this.orbState = 4;
                                        break;
                                    default:
                                        break;
                                }

                                break;


                        }
                    }
                    else
                    {
                        switch (o.type)
                        {
                            case 0:
                                p2.isHealth = true;
                                switch (this.orbState)
                                {
                                    case 1:
                                        hOrb.isConsumable = false;
                                        this.orbState = 0;
                                        break;
                                    case 4:
                                        hOrb.isConsumable = false;
                                        this.orbState = 2;
                                        break;
                                    case 6:
                                        hOrb.isConsumable = false;
                                        this.orbState = 3;
                                        break;
                                    case 7:
                                        hOrb.isConsumable = false;
                                        this.orbState = 5;
                                        break;
                                    default:
                                        break;
                                }

                                break;
                            case 1:
                                superActivate(false);
                                switch (this.orbState)
                                {
                                    case 2:
                                        pOrb.isConsumable = false;
                                        this.orbState = 0;
                                        break;
                                    case 4:
                                        pOrb.isConsumable = false;
                                        this.orbState = 1;
                                        break;
                                    case 5:
                                        pOrb.isConsumable = false;
                                        this.orbState = 3;
                                        break;
                                    case 7:
                                        pOrb.isConsumable = false;
                                        this.orbState = 6;
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case 2:
                                activateShield(false);
                                switch (this.orbState)
                                {
                                    case 3:
                                        sOrb.isConsumable = false;
                                        this.orbState = 0;
                                        break;
                                    case 5:
                                        sOrb.isConsumable = false;
                                        this.orbState = 2;
                                        break;
                                    case 6:
                                        sOrb.isConsumable = false;
                                        this.orbState = 1;
                                        break;
                                    case 7:
                                        sOrb.isConsumable = false;
                                        this.orbState = 4;
                                        break;
                                    default:
                                        break;
                                }

                                break;

                        }
                    }
                }

            }
            

        }
        public void generateOrb()
        {
            int r = rand.Next(3);
            if (r == 0)
            {

                switch (this.orbState)
                {
                    case 0:
                        hOrb.isConsumable = true;
                        pOrb.isConsumable = false;
                        sOrb.isConsumable = false;
                        this.orbState = 1;
                        break;
                    case 2:
                        hOrb.isConsumable = true;
                        pOrb.isConsumable = true;
                        sOrb.isConsumable = false;
                        this.orbState = 4;
                        break;
                    case 3:
                        hOrb.isConsumable = true;
                        pOrb.isConsumable = false;
                        sOrb.isConsumable = true;
                        this.orbState = 6;
                        break;
                    case 5:
                        hOrb.isConsumable = true;
                        pOrb.isConsumable = true;
                        sOrb.isConsumable = true;
                        this.orbState = 7;
                        break;
                    default:
                        break;
                }
            }
            else if (r == 1)
            {
                switch (this.orbState)
                {
                    case 0:
                        hOrb.isConsumable = false;
                        pOrb.isConsumable = true;
                        sOrb.isConsumable = false;
                        this.orbState = 2;
                        break;
                    case 1:
                        hOrb.isConsumable = true;
                        pOrb.isConsumable = true;
                        sOrb.isConsumable = false;
                        this.orbState = 4;
                        break;
                    case 3:
                        hOrb.isConsumable = false;
                        pOrb.isConsumable = true;
                        sOrb.isConsumable = true;
                        this.orbState = 5;
                        break;
                    case 6:
                        hOrb.isConsumable = true;
                        pOrb.isConsumable = true;
                        sOrb.isConsumable = true;
                        this.orbState = 7;
                        break;
                    default:
                        break;
                }
            }
            else if (r == 2)
            {
                switch (this.orbState)
                {
                    case 0:
                        hOrb.isConsumable = false;
                        pOrb.isConsumable = false;
                        sOrb.isConsumable = true;
                        this.orbState = 3;
                        break;
                    case 1:
                        hOrb.isConsumable = true;
                        pOrb.isConsumable = false;
                        sOrb.isConsumable = true;
                        this.orbState = 6;
                        break;
                    case 2:
                        hOrb.isConsumable = false;
                        pOrb.isConsumable = true;
                        sOrb.isConsumable = true;
                        this.orbState = 5;
                        break;
                    case 4:
                        hOrb.isConsumable = true;
                        pOrb.isConsumable = true;
                        sOrb.isConsumable = true;
                        this.orbState = 7;
                        break;
                    default:
                        break;
                }
 
            }
        }
    }
}
