﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;



namespace Battle_of_the_Tanks
{
    class KeyInputManager
    {
        static Dictionary<Keys, bool> cmds = new Dictionary<Keys, bool>();

        public static void setKeyState(Keys key, bool state)
        {
            cmds[key] = state;
        }

        public static bool getKeyState(Keys key)
        {
            if (!cmds.ContainsKey(key))
                return false;

            return cmds[key];

        }

    }
}
