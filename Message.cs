﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace Battle_of_the_Tanks
{
    [Serializable]
    public class Message
    {
        #region Fields
        float x;
        float y;
        int life;
        bool fired;
        int ID;
        static int CurrentID=0;
        public int orbstate;
        public bool p1shield;
        public bool p2shield;
        public bool p1super;
        public bool p2super;
        public bool p1hei;
        public bool p2hei;
        #endregion

        #region Properties
        public int _ID
        {
            get { return this.ID; }
            set { this.ID = value; }
        }
        public float X
        {
            set { this.x = value; }
            get { return this.x; }
        }

        public float Y
        {
            set { this.y = value; }
            get { return this.y; }
        }

        public int LIFE
        {
            set { this.life = value; }
            get { return this.life; }
        }

        public bool FIRED
        {
            set { this.fired = value; }
            get { return this.fired; }
        }
        #endregion
        

        #region Constructor
        public Message(float x, float y, int life, bool fired,int orbstate,bool p1s,bool p2s,bool p1w,bool p2w, bool p1hei , bool p2hei)
        {
            this.x = x;
            this.y = y;
            this.life = life;
            this.fired = fired;
            this.orbstate = orbstate;
            this.p1shield = p1s;
            this.p2shield = p2s;
            this.p1super = p1w;
            this.p2super = p2w;
            this.p1hei = p1hei;
            this.p2hei = p2hei;
            this.ID = ++CurrentID;
        }
        #endregion

        #region Methods
        /// <summary>
        /// A method to send a message of player's status to the other player 
        /// </summary>
        public static void Send(Message message, IPEndPoint receiver, UdpClient client)
        {
            MemoryStream ms = new MemoryStream();

            BinaryFormatter bf = new BinaryFormatter();

            bf.Serialize(ms, message);

            byte[] dataS = ms.GetBuffer();

            try { client.Send(dataS, dataS.Length, receiver); }
            catch (Exception e) { }

        }

       
        public static Message Receive(UdpClient client, ref IPEndPoint sender)
        {
            try { byte[] dataR = client.Receive(ref sender);
            MemoryStream ms = new MemoryStream(dataR);
            BinaryFormatter bf = new BinaryFormatter();

            Message messageR = (Message)bf.Deserialize(ms);
            return messageR;

            }
            catch (Exception e) { }


           

            return new Message(0,0,500,true,0,false,false,false,false,false,false);

        }
        #endregion

       
        


    }
}
