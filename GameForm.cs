﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;
using Battle_of_the_Tanks.GameObject;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Battle_of_the_Tanks
{
    public partial class GameForm : Form
    {
        GamePanel gp;
        public static int screenWidth;
        public static int screenHeight;
        int hpn;
        String ip;
        bool isHost;
        public TcpClient thisEnd;
        public Thread t1;
        public Thread t2;
        public Form home;
        public static Form IbwdForm;
        public static Form thisGame;
        public Message incomingMess;
        public GameForm(int hpn,Form home)
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
            this.isHost = true;
            this.hpn = hpn;
            this.home = home;
            IbwdForm = this.home;
            thisGame = this;
            FormBorderStyle = FormBorderStyle.None;
            screenWidth = Screen.PrimaryScreen.Bounds.Width;
            screenHeight = Screen.PrimaryScreen.Bounds.Height;
            this.Bounds = new Rectangle(0, 0, screenWidth, screenHeight);
            Player1Health.Location = new Point((int)(0.599 * screenWidth), (int)(0.93 * screenHeight));
            Player1Health.Size = new System.Drawing.Size((int)(0.4 * screenWidth), (int)(0.07 * screenHeight));
            Player2Health.Location = new Point(0, (int)(0.93 * screenHeight));
            Player2Health.Size = new System.Drawing.Size((int)(0.4 * screenWidth), (int)(0.07 * screenHeight));
            gp = new GamePanel(this,this.isHost);
            this.Controls.Add(gp);
            this.KeyDown += GameForm_KeyDown;
            this.KeyUp += GameForm_KeyUp;
            hostComm(hpn);
        }
        public GameForm(int hpn,String ip,Form home)
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
            this.isHost = false;
            this.hpn = hpn;
            this.home = home;
            IbwdForm = this.home;
            this.ip = ip;
            FormBorderStyle = FormBorderStyle.None;
            screenWidth = Screen.PrimaryScreen.Bounds.Width;
            screenHeight = Screen.PrimaryScreen.Bounds.Height;
            this.Bounds = new Rectangle(0, 0, screenWidth, screenHeight);
            Player1Health.Location = new Point((int)(0.599 * screenWidth), (int)(0.93 * screenHeight));
            Player1Health.Size = new System.Drawing.Size((int)(0.4 * screenWidth), (int)(0.07 * screenHeight));
            Player2Health.Location = new Point(0, (int)(0.93 * screenHeight));
            Player2Health.Size = new System.Drawing.Size((int)(0.4 * screenWidth), (int)(0.07 * screenHeight));
            gp = new GamePanel(this, this.isHost);
            this.Controls.Add(gp);
            this.KeyDown += GameForm_KeyDown;
            this.KeyUp += GameForm_KeyUp;
            clientComm();
        }
        public void dropHealth1(int x)
        {
            Player1Health.Increment(x);
        }
        public void dropHealth2(int x)
        {
            Player2Health.Increment(x);
        }
        public void hostComm(int hpn)
        {
            try {
                TcpListener hostListener = new TcpListener(hpn);
                hostListener.Start();
                thisEnd = hostListener.AcceptTcpClient();
                t1 = new Thread(hostCommunicate);
                t1.Start();
                hostListener.Stop();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public void clientComm()
        {
            try {
                thisEnd = new TcpClient(ip, hpn);
                t2 = new Thread(clientCommunicate);
                t2.Start();
                
            }
            catch(Exception e)

            {
                Console.WriteLine(e.Message);
            }
        }
        void GameForm_KeyUp(object sender, KeyEventArgs e)
        {
            KeyInputManager.setKeyState(e.KeyCode, false);
        }

        void GameForm_KeyDown(object sender, KeyEventArgs e)
        {
            KeyInputManager.setKeyState(e.KeyCode, true);
        }

        private void GameForm_Load(object sender, EventArgs e)
        {
          
        }
        public void hostCommunicate()
        {
            try
            { UdpClient player1 = new UdpClient(6667);
                IPEndPoint end = new IPEndPoint(IPAddress.Any, 0);

          
                while (true)
                {
              
                    Message.Send(new Message(gp.p1.X, gp.p1.Y, gp.p2.Life, KeyInputManager.getKeyState(Keys.Space), gp.orbState, gp.p1.isShielded, gp.p2.isShielded, gp.p1.superWep, gp.p2.superWep,gp.p1.isHealth,gp.p2.isHealth), end, player1);
             
                    incomingMess = Message.Receive(player1, ref end);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void clientCommunicate()
        {
            try
            {
                UdpClient player2 = new UdpClient();
            IPEndPoint hostEnd = new IPEndPoint(IPAddress.Parse(ip), 6667);

           
            while (true)
            {
     

                incomingMess = Message.Receive(player2, ref hostEnd);
                Message.Send(new Message(gp.p2.X, gp.p2.Y, gp.p1.Life, KeyInputManager.getKeyState(Keys.Space), gp.orbState, gp.p1.isShielded, gp.p2.isShielded,gp.p1.superWep,gp.p2.superWep, gp.p1.isHealth, gp.p2.isHealth), hostEnd, player2);
            }
            
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
       
    }
}
